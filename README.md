# Ansible EE Netbox Inventory

## Build
```sh
ansible-builder build --tag registry.gitlab.com/doxic/ansible-ee-netbox-inventory:latest
docker login registry.gitlab.com
docker push registry.gitlab.com/doxic/ansible-ee-netbox-inventory
```

docker login registry.gitlab.com -u doxic -p TOKEN